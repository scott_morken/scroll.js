/**
 * Created by smorken on 4/21/15.
 */
var smorken = smorken || {};

smorken.scroll = {};
/**
 * Created by smorken on 4/24/15.
 */
smorken.scroll.Scroll = function (options) {
    this.view = null;
    this.indicator = null;
    this.parent = null;
    this.relative = null;
    this.min = 0;
    this.max = 0;
    this.offset = 0;
    this.reference = null;
    this.pressed = false;
    this.xform = null;
    this.options = {
        view: '#view',
        indicator: '#indicator',
        parent: window,
        propagate: false
    };

    this.setOptions(options);
    this.setParent(this.getOption('parent'));
    this.setView(this.getOption('view'));
    this.setIndicator(this.getOption('indicator'));
    this.setMax(this.view, this.parent);
    this.setRelative(30, this.parent);
    this.createListeners(this.view);
    this.setTransform(this.view, 'transform', ['webkit', 'Moz', 'O', 'ms']);
};

smorken.scroll.Scroll.prototype = {
    setOptions: function (options) {
        var self = this;
        smorken.each(options, function (k, v) {
            if (self.hasOwnProperty(k) && !self.options.hasOwnProperty(k)) {
                self[k] = v;
            }
            else {
                self.options[k] = v;
            }
        });
    },
    getOption: function (key, _default) {
        if (typeof this.options[key] !== 'undefined') {
            return this.options[key];
        }
        return (typeof _default !== 'undefined') ? _default : null;
    },
    setParent: function (parentElement) {
        this.setElement('parent', parentElement);
    },
    setView: function (viewElement) {
        this.setElement('view', viewElement);
    },
    setIndicator: function (indicatorElement) {
        this.setElement('indicator', indicatorElement);
    },
    setElement: function (which, ele) {
        if (typeof ele !== "string") {
            this[which] = ele;
        }
        else {
            this[which] = document.querySelector(ele);
        }
    },
    setMax: function (view, parent) {
        this.max = this.getHeight(view) - this.getHeight(parent);
    },
    setRelative: function (offset, parent) {
        offset = offset || 30;
        this.relative = (this.getHeight(parent) - offset) / this.max;
    },
    setTransform: function (view, _default, others) {
        var self = this;
        others.every(function (prefix) {
            var e = prefix + 'Transform';
            if (typeof view.style[e] !== 'undefined') {
                self.xform = e;
                return false;
            }
            return true;
        });
    },
    getHeight: function(ele) {
        if (!ele) {
            return 0;
        }
        if (ele instanceof Element) {
            return parseInt(getComputedStyle(ele).height, 10);
        }
        if (ele.hasOwnProperty('innerHeight')) {
            return parseInt(ele.innerHeight, 10);
        }
        return parseInt(ele.height, 10);
    },
    createListeners: function (view) {
        var self = this;
        if (typeof window.ontouchstart !== 'undefined') {
            smorken.Event.bind(view, 'touchstart', function (e) {
                return self.tap(e);
            });
            smorken.Event.bind(view, 'touchmove', function (e) {
                return self.drag(e);
            });
            smorken.Event.bind(view, 'touchend', function (e) {
                return self.release(e);
            });
        }
        smorken.Event.bind(view, 'mousedown', function (e) {
            return self.tap(e);
        });
        smorken.Event.bind(view, 'mousemove', function (e) {
            return self.drag(e);
        });
        smorken.Event.bind(view, 'mouseup', function (e) {
            return self.release(e);
        });
    },
    ypos: function (e) {
        //touch event
        if (e.targetTouches && (e.targetTouches.length >= 1)) {
            return e.targetTouches[0].clientY;
        }
        //mouse event
        return e.clientY;
    },
    scroll: function (y) {
        this.offset = (y > this.max) ? this.max : (y < this.min) ? this.min : y;
        this.view.style[this.xform] = 'translateY(' + (-this.offset) + 'px)';
        if (this.indicator) {
            this.indicator.style[this.xform] = 'translateY(' + (this.offset * this.relative) + 'px)';
        }
    },
    tap: function (e) {
        this.pressed = true;
        this.reference = this.ypos(e);
        return true;
    },
    drag: function (e) {
        var y, delta;
        if (this.pressed) {
            y = this.ypos(e);
            delta = this.reference - y;
            if (delta > 2 || delta < -2) {
                this.reference = y;
                this.scroll(this.offset + delta);
            }
        }
        return this.propagateEvent(e);
    },
    release: function (e) {
        this.pressed = false;
        return true;
    },
    propagateEvent: function(e) {
        var prop = this.getOption('propagate', false);
        if (!prop) {
            e.preventDefault();
            e.stopPropagation();
        }
        return prop;
    }
};