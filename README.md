Many of the ideas found here are courtesy of [Kinetic Scrolling with JavaScript](https://github.com/ariya/kinetic/)

### Bower Install

```
{
  "name": "my-project",
  "dependencies": {
    "scroll.js": "git+https://bitbucket.org/scott_morken/scroll.js.git#~0.0.1"
  }
}
```

### Gulpfile addition

```
...
  .copy('./node_modules/smorken.js/dist', './public/js/limited')
  .copy('./node_modules/scroll.js/dist', './public/js/limited')
...
```
